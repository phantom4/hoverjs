hover.js 0.1.0
============================
オンマウスでフェードを実行するJavaScript。



仕様
----
* デスクトップのとき、aタグのオンマウスでフェードする。
* タブレット、スマートフォンでは実行しない。

 
使い方
------
### Step1: ファイルの読み込み

```html
<script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="js/hover.min.js"></script>
```

### Step2: マークアップ
```html
<a data-hover="1">リンク</a>
<a data-hover="2">
    <span class="hover">リンク</a>
</a>
```



data属性
--------

### data-hover
フェードする要素を指定する

```html
<a data-hover="1">aタグがフェードの対象になる</a>
```

```html
<a data-hover="2">
    <span class="hover">aタグ内のhoverクラスが設定された要素がフェードの対象になる</span>
</a>
```

### data-hover-to
オンマウス後のアルファ値

```html
<a data-hover="1" data-hover-to="0.2">オンマウスで透明度20%になる</a>
<a data-hover="2" data-hover-to="0.2">
    <span class="hover">オンマウスでaタグ内のhoverクラスが設定された要素が透明度20%になる</span>
</a>
```

### data-hover-to-duration
オンマウス後のアニメーション効果時間

```html
<a data-hover="1" data-hover-to-duration="1">オンマウスで1秒でフェードする</a>
<a data-hover="2" data-hover-to-duration="1">
    <span class="hover">オンマウスでaタグ内のhoverクラスが設定された要素が、1秒でフェードする</span>
</a>
```

### data-hover-from
オンマウス前のアルファ値

```html
<a data-hover="1" data-hover-from="0.2">オンマウス前の透明度が20%になる</a>
<a data-hover="2" data-hover-from="0.2">
    <span class="hover">オンマウス前のaタグ内のhoverクラスが設定された要素が透明度20%になる</span>
</a>
```

### data-hover-from-duration
マウスアウト後のアニメーション効果時間

```html
<a data-hover="1" data-hover-to-duration="1">マウスアウトで1秒でフェードする</a>
<a data-hover="2" data-hover-to-duration="1">
    <span class="hover">マウスアウトでaタグ内のhoverクラスが設定された要素が、1秒でフェードする</span>
</a>
```



ライセンス
----------
Copyright &copy; 2015