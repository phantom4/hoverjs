###*
 * オンマウスエフェクト
 *
 * @param $ {jQuery}
###
do ($ = jQuery) ->

  $ () ->
    #----------------------------------
    #  elements
    #----------------------------------
    $html = $("html")
    $body = $("body")

    #----------------------------------
    #  variables
    #----------------------------------
    ua = navigator.useaAgent
    isLteIE8 = $html.hasClass("lt-ie9")  #IE8以下か
    # isSmartPhone = device.mobile()  #スマートフォンか
    # isTablet = device.tablet()  #タブレットか
    isSmartPhone = ua.indexOf("iPhone") > 0 || ua.indexOf("iPod") > 0 || ua.indexOf("Android") > 0 && ua.indexOf("Mobile")  #スマートフォンか
    isTablet = ua.indexOf("iPad") > 0 || ua.indexOf("Android") > 0 && !ua.indexOf("Mobile")  #タブレットか


    #要素そのものをフェードするタイプ
    $body
    .find("a[data-hover=1]").each( ->
      #初期のアルファを反映する
      $this = $(this)
      dest = $this.attr("data-hover-from")

      $this.css({
        opacity: dest
      })
      return
    )
    .end()
    .find("a[data-hover=2]").each( ->
      #初期のアルファを反映する
      $this = $(this)
      $hover = $this.find(".hover")

      $hover.css({
        opacity: 0
      })
      return
    )


    #ロールオーバー
    if isSmartPhone == false && isTablet == false
      #デスクトップなら

      if isLteIE8 == false
        #IE9以上 or 他のブラウザ
        $body
        .on("mouseenter", "a[data-hover=1]", () ->
          $this = $(this)
          dest = $this.attr("data-hover-to")
          duration = $this.attr("data-hover-to-duration")

          $this
          .velocity("stop")
          .velocity({
            opacity: if isNaN(dest) == false then dest else .7
          }, {
            duration: if isNaN(duration) == false then duration else 400
            easing: "easeOutQuad"
          })
          return

        ).on("mouseout", "a[data-hover=1]", () ->
          $this = $(this)
          dest = $this.attr("data-hover-from")
          duration = $this.attr("data-hover-from-duration")

          $this
          .velocity("stop")
          .velocity({
            opacity: if isNaN(dest) == false then dest else 1
          }, {
            duration: if isNaN(duration) == false then duration else 200
            easing: "easeOutQuad"
          })
          return
        )


        #中のdivをフェードするタイプ
        $body
        .on("mouseenter", "a[data-hover=2]", () ->
          $this = $(this)
          $hover = $this.find(".hover")
          duration = $this.attr("data-hover-to-duration")

          $hover
          .velocity("stop")
          .velocity({
            opacity: 1
          }, {
            duration: if isNaN(duration) == false then duration else 400
            easing: "easeOutQuad"
          })
          return
        )
        .on("mouseleave", "a[data-hover=2]", () ->
          $this = $(this)
          $hover = $this.find(".hover")
          duration = $this.attr("data-hover-from-duration")

          $hover
          .velocity("stop")
          .velocity({
            opacity: 0
          }, {
            duration: if isNaN(duration) == false then duration else 200
            easing: "easeOutQuad"
          })
          return
        )

      else
        #IE8以外のデスクトップブラウザ
        #アニメーションをしない（アニメーションすると、pngの透過部分が黒くなるため）
        $body
        .on("mouseenter", "a[data-hover=1]", () ->
          $this = $(this)
          dest = $this.attr("data-hover-to")

          $this.css({
            opacity: if isNaN(dest) == false then dest else .7
          })
          return
        )
        .on("mouseleave", "a[data-hover=1]", () ->
          $this = $(this)
          dest = $this.attr("data-hover-from")

          $this.css({
            opacity: if isNaN(dest) == false then dest else 1
          })
          return
        )


        #中のdivをフェードするタイプ
        $("body")
        .on("mouseenter", "a[data-hover=2]", () ->
          $hover = $(this).find(".hover")

          $hover.css({
            opacity: 1
          })
          return
        )
        .on("mouseenter", "a[data-hover=2]", () ->
          $hover = $(this).find(".hover")

          $hover.css({
            opacity: 0
          })
          return
        )

    return

  return