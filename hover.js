
/**
 * オンマウスエフェクト
 *
 * @param $ {jQuery}
 */
(function($) {
  $(function() {
    var $body, $html, isLteIE8, isSmartPhone, isTablet, ua;
    $html = $("html");
    $body = $("body");
    ua = navigator.useaAgent;
    isLteIE8 = $html.hasClass("lt-ie9");
    isSmartPhone = ua.indexOf("iPhone") > 0 || ua.indexOf("iPod") > 0 || ua.indexOf("Android") > 0 && ua.indexOf("Mobile");
    isTablet = ua.indexOf("iPad") > 0 || ua.indexOf("Android") > 0 && !ua.indexOf("Mobile");
    $body.find("a[data-hover=1]").each(function() {
      var $this, dest;
      $this = $(this);
      dest = $this.attr("data-hover-from");
      $this.css({
        opacity: dest
      });
    }).end().find("a[data-hover=2]").each(function() {
      var $hover, $this;
      $this = $(this);
      $hover = $this.find(".hover");
      $hover.css({
        opacity: 0
      });
    });
    if (isSmartPhone === false && isTablet === false) {
      if (isLteIE8 === false) {
        $body.on("mouseenter", "a[data-hover=1]", function() {
          var $this, dest, duration;
          $this = $(this);
          dest = $this.attr("data-hover-to");
          duration = $this.attr("data-hover-to-duration");
          $this.velocity("stop").velocity({
            opacity: isNaN(dest) === false ? dest : .7
          }, {
            duration: isNaN(duration) === false ? duration : 400,
            easing: "easeOutQuad"
          });
        }).on("mouseout", "a[data-hover=1]", function() {
          var $this, dest, duration;
          $this = $(this);
          dest = $this.attr("data-hover-from");
          duration = $this.attr("data-hover-from-duration");
          $this.velocity("stop").velocity({
            opacity: isNaN(dest) === false ? dest : 1
          }, {
            duration: isNaN(duration) === false ? duration : 200,
            easing: "easeOutQuad"
          });
        });
        $body.on("mouseenter", "a[data-hover=2]", function() {
          var $hover, $this, duration;
          $this = $(this);
          $hover = $this.find(".hover");
          duration = $this.attr("data-hover-to-duration");
          $hover.velocity("stop").velocity({
            opacity: 1
          }, {
            duration: isNaN(duration) === false ? duration : 400,
            easing: "easeOutQuad"
          });
        }).on("mouseleave", "a[data-hover=2]", function() {
          var $hover, $this, duration;
          $this = $(this);
          $hover = $this.find(".hover");
          duration = $this.attr("data-hover-from-duration");
          $hover.velocity("stop").velocity({
            opacity: 0
          }, {
            duration: isNaN(duration) === false ? duration : 200,
            easing: "easeOutQuad"
          });
        });
      } else {
        $body.on("mouseenter", "a[data-hover=1]", function() {
          var $this, dest;
          $this = $(this);
          dest = $this.attr("data-hover-to");
          $this.css({
            opacity: isNaN(dest) === false ? dest : .7
          });
        }).on("mouseleave", "a[data-hover=1]", function() {
          var $this, dest;
          $this = $(this);
          dest = $this.attr("data-hover-from");
          $this.css({
            opacity: isNaN(dest) === false ? dest : 1
          });
        });
        $("body").on("mouseenter", "a[data-hover=2]", function() {
          var $hover;
          $hover = $(this).find(".hover");
          $hover.css({
            opacity: 1
          });
        }).on("mouseenter", "a[data-hover=2]", function() {
          var $hover;
          $hover = $(this).find(".hover");
          $hover.css({
            opacity: 0
          });
        });
      }
    }
  });
})(jQuery);
